package com.journaldev.mvpdagger2retroiftrxjava.pojo;

import com.google.gson.annotations.SerializedName;

public class CryptoData {

    @SerializedName("postId")
    public String postId;
    @SerializedName("id")
    public String id;
    @SerializedName("name")
    public String name;
    @SerializedName("email")
    public String email;
    @SerializedName("body")
    public String body;

}
